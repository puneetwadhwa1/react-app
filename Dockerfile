
#FROM ubuntu:latest

# Getting Base Image\
FROM node:16

MAINTAINER puneet wadhwa <puneetwadhwa@fynd.com>


# Change working directory
WORKDIR /usr/src/app

COPY package*.json ./

# Copy source code
COPY . .



# Install dependencies
RUN npm install


# Expose API port to the outside
EXPOSE 3001

# # Launch application
 CMD ["npm","start"]